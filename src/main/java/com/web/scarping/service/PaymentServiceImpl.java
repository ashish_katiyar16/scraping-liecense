package com.web.scarping.service;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Base64;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;
import com.paypal.api.payments.Payment;
import com.stripe.model.Charge;
import com.web.scarping.dao.LicenceRepo;
import com.web.scarping.dao.OrderRepo;
import com.web.scarping.dao.PaypalPaymentRepo;
import com.web.scarping.dto.ChargeRequest;
import com.web.scarping.dto.Order;
import com.web.scarping.dto.OrderResponseDTO;
import com.web.scarping.model.LicenceModel;
import com.web.scarping.model.OrderModel;
import com.web.scarping.model.PaypalPaymentModel;
import com.web.scarping.util.Util;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private OrderRepo orderRepo;
	@Autowired
	private PaypalPaymentRepo paypalPaymentRepo;
	@Autowired
	private LicenceRepo licenceRepo;
	
	@Autowired
	private MailSenderService mailSenderService;
	
	@Value("${email.download.link.baseurl}")
	private String DOWNLOAD_BASE_URL;
	
	@Value("${spring.mail.properties.mail.smtp.starttls.enable}")
	private Boolean mailByCredentials;
	
	@Value("${pdf.location}")
	private String PDF_LOCATION;

	@Override
	public OrderResponseDTO getOrderdetails(String pId) {
		OrderResponseDTO paymentResponseDTO = null;
		try {
			OrderModel order = orderRepo.getOrder(pId);
			if (order != null) {
				LicenceModel licence = licenceRepo.getLicence(order.getMcId());
				paymentResponseDTO = new OrderResponseDTO();
				paymentResponseDTO.setPaymentId(order.getId());
				paymentResponseDTO.setLegalName(licence.getLegalName());
				paymentResponseDTO.setMailingaddress(licence.getMailingaddress());
				paymentResponseDTO.setPhysicalAddress(licence.getPhysicalAddress());
				paymentResponseDTO.setMcId(order.getMcId());
				paymentResponseDTO.setPhone(licence.getPhone());
				paymentResponseDTO.setUsDot(licence.getUsDot());
				paymentResponseDTO.setApproveDate(licence.getApprovalDate());
				
				// if expired the the send expire other what ever status is
				System.out.println("Expiry Date:"+order.getExpiryDate());
				System.out.println("Current Date:"+Util.getCurrentDate(0));
				boolean compareDate = Util.compareDate(LocalDate.parse(order.getExpiryDate()),
						LocalDate.parse(Util.getCurrentDate(0)));
				if (!compareDate) {
					paymentResponseDTO.setStatus("EXPIRED");
				} else {
					if(order.getStatus().equalsIgnoreCase("SUCCESS")){
						paymentResponseDTO.setPaymentId(order.getPaymentId());
					}
					paymentResponseDTO.setStatus(order.getStatus());
				}

			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return paymentResponseDTO;
	}

	@Override
	public String updateOrderStatus(String id, String paypalResponse, String status, String paymentId) {
		String result = "FAILED";
		try {
			OrderModel order = orderRepo.getOrder(id);
			order.setPaypalResponse(paypalResponse);
			order.setPaymentId(paymentId);
			order.setStatus(status);
			orderRepo.update(order);
			result = "SUCCESS";
		} catch (Exception e) {
			System.out.println("Exception in Order Update :" + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public void createPaypalPayment(Order order, String id) {
		try {
			PaypalPaymentModel paymentModel = new PaypalPaymentModel();
			paymentModel.setId(id);
			paymentModel.setEmail(order.getEmailId());
			paymentModel.setOrderId(order.getOrderId());
			paypalPaymentRepo.save(paymentModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public PaypalPaymentModel getPaypalPaymentDetails(String paymentId) {
		try {
			return paypalPaymentRepo.getPayment(paymentId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updatePaypalPayment(String id, Payment payment) {
		try {
			PaypalPaymentModel paymentModel = paypalPaymentRepo.getPayment(id);
			paymentModel.setPaymentMethod(payment.getPayer().getPaymentMethod());
			paymentModel.setPaymentDate(Util.getCurrentDate(0));
			paymentModel.setStatus(payment.getState());
			paypalPaymentRepo.update(paymentModel);
		} catch (Exception e) {
			System.out.println("Exception in Payment Update :" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Autowired
	private JavaMailsenderService javaMailsenderService;
	
	@Async
	public void sendEmailtoUser(String paymentId) {
		try {
			PaypalPaymentModel paypalPaymentModel = getPaypalPaymentDetails(paymentId);
			if(paypalPaymentModel!=null){
				OrderResponseDTO orderDetails = getOrderdetails(paypalPaymentModel.getOrderId());
				if(orderDetails!=null){
					String newline = " \n ";
					String message = "Hi "+orderDetails.getLegalName()+newline+newline
				    +"Thanks for purchasing your motor carrier certificate. Click the link to download your certificate."+newline+newline
					+"Thank you for your order!"+newline
					+"Please keep this order ID for future reference."+newline+newline
					+"YOUR ORDER ID NUMBER IS: "+paypalPaymentModel.getOrderId()+newline+newline
					+"---------------------------------------------------------------------------"+newline
					+"TruckersFinder.com"+newline
					+"---------------------------------------------------------------------------"+newline
					+"Order ID Number: "+paypalPaymentModel.getOrderId()+newline
					+"Order Date: "+Util.getFormattedDate(paypalPaymentModel.getPaymentDate())+newline+newline
					+"BILL ADDRESS:"+newline
					+orderDetails.getPhysicalAddress()+newline+newline
					+"Payment Method: "+paypalPaymentModel.getPaymentMethod()+newline
					+"Ship Via:"+newline
					+"E-Mail Address: contact@truckersfinder.com"+newline
					+"ORDER INVENTORY:"+newline
					+"Product: MC-"+orderDetails.getMcId()+"-C.pdf"+newline
					+"Quantity: 1"+newline
					+"Price:  $14.99"+newline
					+"Net Invoice: $14.99"+newline
					+"Tax: $0.00"+newline
					+"Shipping: $0.00"+newline
					+"Invoice Total: $14.99"+newline+newline
					+newline+newline+newline+"";
					javaMailsenderService.sendMailWithAttachment(paypalPaymentModel.getEmail(), "Motor Carrier Certificate", message, "MC-"+orderDetails.getMcId()+"-C.pdf");
					/*if(mailByCredentials){
			    		mailSenderService.sendMail(paypalPaymentModel.getEmail(), message,"Motor Carrier Certificate");
			    	}else{
			    		mailSenderService.sendMailBySmtp(paypalPaymentModel.getEmail(), message,"Motor Carrier Certificate");
			    	}*/
				}
			}
		} catch (Exception e) {
			System.out.println("Exception in sendEmailtoUser :" + e.getMessage());
			e.printStackTrace();
		}
		
	}

	@Override
	public void updateStripePayment(String id, Charge charge) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PaypalPaymentModel createPaypalPaymentStripe(Charge charge, ChargeRequest chargeRequest) {
		PaypalPaymentModel paypalPaymentModel = new PaypalPaymentModel();
		paypalPaymentModel.setEmail(chargeRequest.getStripeEmail());
		paypalPaymentModel.setId(charge.getId());
		paypalPaymentModel.setOrderId(chargeRequest.getOrderId());
		paypalPaymentModel.setTransactionId(charge.getBalanceTransaction());
		paypalPaymentModel.setPaymentDate(Util.getCurrentDate(0));
		paypalPaymentModel.setPaymentMethod(charge.getPaymentMethodDetails().getType());
		paypalPaymentModel.setStatus("approved");
		paypalPaymentRepo.save(paypalPaymentModel);
		OrderModel order = orderRepo.getOrder(chargeRequest.getOrderId());
		order.setPaypalResponse("approved");
		order.setPaymentId(charge.getId());
		order.setStatus("SUCCESS");
		orderRepo.update(order);
		return paypalPaymentModel;
	}
	
	@Autowired
	private TemplateEngine templateEngine;

	@Override
	public void savepdf(String id) {
		try {
			PaypalPaymentModel paypalPaymentModel = getPaypalPaymentDetails(id);
			if (paypalPaymentModel != null) {
				if (paypalPaymentModel.getStatus().equalsIgnoreCase("approved")) {
					
					OrderResponseDTO paymentResponseDTO = getOrderdetails(paypalPaymentModel.getOrderId());
					if (paymentResponseDTO != null && paymentResponseDTO.getStatus().equalsIgnoreCase("SUCCESS")) {
						Context ctx = new Context();
						ctx.setVariable("approveDate", Util.getFormattedDate(paymentResponseDTO.getApproveDate()));
						ctx.setVariable("mcId", "MC-" + paymentResponseDTO.getMcId() + "-C");
						ctx.setVariable("usdotNo", "U.S. DOT No. " + paymentResponseDTO.getUsDot());
						ctx.setVariable("leagalName", paymentResponseDTO.getLegalName());
						ctx.setVariable("address", Util.formatMailingAddress(paymentResponseDTO.getPhysicalAddress()));
						ctx.setVariable("logoImage", "data:image/png;base64, " + convertToBase64("logo.png"));
						ctx.setVariable("signImage", "data:image/png;base64, " + convertToBase64("signature.png"));
						
						String processedHtml = templateEngine.process("updatedpdf", ctx);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						
						ITextRenderer renderer = new ITextRenderer();
				        renderer.setDocumentFromString(processedHtml);
				        renderer.layout();
				        try {
							renderer.createPDF(out, false);
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				        renderer.finishPDF();
				        FileOutputStream fout=new FileOutputStream(PDF_LOCATION+"MC-"+paymentResponseDTO.getMcId()+"-C.pdf");    
			            fout.write(out.toByteArray());    
			            fout.close();    
				        
						
					}
					
				}

			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private String convertToBase64(String fileName) {
	    byte[] imageAsBytes = new byte[0];
	    try {
	      ClassPathResource imgFile = new ClassPathResource("/" + fileName);
	      InputStream inputStream = imgFile.getInputStream();
	      imageAsBytes = IOUtils.toByteArray(inputStream);
	    } catch (IOException e) {
	      System.out.println("\n File read Exception");
	    }
	    return Base64.getEncoder().encodeToString(imageAsBytes);
	  }

}
