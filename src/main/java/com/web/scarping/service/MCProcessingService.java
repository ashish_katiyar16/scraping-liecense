package com.web.scarping.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.web.scarping.dao.LicenceRepo;
import com.web.scarping.dao.OrderRepo;
import com.web.scarping.model.LicenceModel;
import com.web.scarping.model.OrderModel;
import com.web.scarping.util.Util;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MCProcessingService {
	@Autowired
	private ReadMasterDataService readMasterDataService;

	@Autowired
	private SearchMCLicentService searchMCLicentService;

	@Autowired
	private LicenceRepo dynamoCRUD;

	@Autowired
	private OrderRepo paymentRepo;

	@Value("${sns.message.sent.enabled}")
	private Boolean isEnabled;
	@Autowired
	private SMSsenderService SMSsenderService;

	@Value("${messsage.template}")
	private String messagetemplate;

	@Value("${link.baseurl}")
	private String linkBaseURL;
	
	@Value("${send.for.prev.date}")
	private Boolean sendForPreviousDate;
	
	@Value("${previous.dates.for.sms}")
	private String previousDates;

	@Scheduled(cron = "${scheduler.cron}")
	public void processMCLicense() {
		try {
			List<String> prevDates=new ArrayList<String>();
			if(previousDates!=null && !previousDates.equalsIgnoreCase("")){
				String dateArr[] = previousDates.split(",");
				for(String date : dateArr){
					prevDates.add(date);
				}
			}
			List<String> licenceData = readMasterDataService.getLicenceData();
			// System.out.println("Master data: " + licenceData);
			for (String lc : licenceData) {
				try {
					// log.info("License Number : " + lc);
					// System.out.println("license Number : " + lc);
					String[] arr = lc.split("-");
					// System.out.println(arr.length);
					if (arr.length >= 4) {
						String searchKey = arr[arr.length - 2];
						LicenceModel existLicenceModel = dynamoCRUD.getLicence(searchKey);
						if (existLicenceModel != null) {
							log.info("Licence number already present in db for key  {}", searchKey);
							if(sendForPreviousDate && prevDates.size()>0){
								if (prevDates.contains(existLicenceModel.getApprovalDate())) {
									log.info("Message sent to Phone PREV: Sending sms for Date :{}",existLicenceModel.getApprovalDate());
									OrderModel paymentModel = new OrderModel();
									paymentModel.setId(Util.generatePaymentId());
									paymentModel.setMcId(existLicenceModel.getMcId());
									paymentModel.setExpiryDate("" + Util.getCurrentDate(7));
									paymentModel.setStatus("PENDING");
									try {
										paymentRepo.save(paymentModel);
									} catch (Exception e) {
										e.printStackTrace();
									}
									String formattedPhoneNo = Util.formatPhoneNumber(existLicenceModel.getPhone());
									if (isEnabled) {
										String stringBuilder = messagetemplate.replaceAll("<businessname>",
												existLicenceModel.getLegalName());
										String finalLink = linkBaseURL.concat(paymentModel.getId());
										stringBuilder = stringBuilder.replaceAll("<link>", finalLink);
										SMSsenderService.sendSMSMessage(stringBuilder, formattedPhoneNo);

									}
								}else{
									log.info("Message sent to Phone PREV: Not in allowed Date :{}",existLicenceModel.getApprovalDate());
								}
							}
							continue;
						}
						String date = arr[0] + "-" + arr[1] + "-" + arr[2];
						// System.out.println("searchKey: " + searchKey);
						Map<String, String> licenceDetails = searchMCLicentService.getLicenceDetails(searchKey);
						if (licenceDetails.size() != 0) {
							LicenceModel licenceModel = new LicenceModel();
							OrderModel paymentModel = new OrderModel();
							licenceModel.setLegalName(licenceDetails.get("Legal Name:"));
							licenceModel.setMcId(searchKey);
							licenceModel.setPhone(licenceDetails.get("Phone:"));
							licenceModel.setMailingaddress(licenceDetails.get("Mailing Address:"));
							licenceModel.setPhysicalAddress(licenceDetails.get("Physical Address:"));
							licenceModel.setUsDot(licenceDetails.get("USDOT Number:"));
							licenceModel.setApprovalDate(date);
							try {
								dynamoCRUD.save(licenceModel);
							} catch (Exception e) {
								e.printStackTrace();
							}
							paymentModel.setId(Util.generatePaymentId());
							paymentModel.setMcId(licenceModel.getMcId());
							paymentModel.setExpiryDate("" + Util.getCurrentDate(7));
							paymentModel.setStatus("PENDING");
							try {
								paymentRepo.save(paymentModel);
							} catch (Exception e) {
								e.printStackTrace();
							}
							log.info("Detail for Licensekey {} is {}: ", searchKey, licenceModel);
							// System.out.println("Detail for Licensekey " + searchKey + " is" +
							// licenceModel);
                            String formattedPhoneNo = Util.formatPhoneNumber(licenceModel.getPhone());
							if (isEnabled) {
								boolean allow = Util.checkApprovalDate(licenceModel.getApprovalDate());
								if(allow){
									String stringBuilder = messagetemplate.replaceAll("<businessname>",
											licenceModel.getLegalName());
									String finalLink = linkBaseURL.concat(paymentModel.getId());
									stringBuilder = stringBuilder.replaceAll("<link>", finalLink);
									SMSsenderService.sendSMSMessage(stringBuilder, formattedPhoneNo);
								}else{
									log.info("Approval Date for Licence No : "+licenceModel.getMcId()+" is more than 5 days old: "+licenceModel.getApprovalDate()+". So sms will not sent");
								}
								
							}

						} else {
							log.info("License details not found for license key:  {}", searchKey);
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}