package com.web.scarping.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SearchMCLicentServiceImpl implements SearchMCLicentService {
	@Value("${mc.search.url}")
	private String searchURl;
	final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
	private List<String> dataKeys = new ArrayList<>(
			Arrays.asList("Entity Type:", "Operating Status:", "Legal Name:", "Legal Name:", "Physical Address:",
					"MC/MX/FF Number(s):", "MCS-150 Form Date:", "Phone:", "Mailing Address:", "USDOT Number:"));

	public Map<String, String> getLicenceDetails(String searchKey) {
		Map<String, String> dataMap = new HashMap<>();
		try {
			// # Go to login page
			Connection.Response loginFormResponse = Jsoup.connect(searchURl).method(Connection.Method.GET)
					.timeout(10 * 1000).userAgent(USER_AGENT).execute();

			// # Fill the login form
			// ## Find the form first...
			FormElement loginForm = (FormElement) loginFormResponse.parse().select("form").first();
			checkElement("Login Form", loginForm);
			Element radioButton1 = loginForm.select("#1").first();
			radioButton1.removeAttr("checked");
			
			Element radioButton = loginForm.select("#2").first();
			checkElement("Login Field", radioButton);
			radioButton.val("MC_MX");
			radioButton.attr("checked","checked");

			// ## ... then "type" the username ...
			Element searchKeyElement = loginForm.select("#4").first();
			checkElement("Login Field", searchKeyElement);
			searchKeyElement.val(searchKey);

			// # Now send the form for login
			Connection.Response loginActionResponse = null;
			try {
				loginActionResponse = loginForm.submit().cookies(loginFormResponse.cookies()).timeout(10 * 1000)
						.userAgent(USER_AGENT).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Elements body = loginActionResponse.parse().select("body");
			for (Element element : body) {
				Elements tables = element.select("table");
				for (Element table : tables) {
					Elements trs = table.select("tr");
					for (Element tr : trs) {
						String th = tr.select("th").text();
						if(th.equalsIgnoreCase("USDOT Number: State Carrier ID Number:")){
							th = "USDOT Number:";
						}
						String td = tr.select("td").text();
						if (dataKeys.contains(th.trim())) {
							dataMap.put(th.trim(), td.trim());
							// System.err.println(dataMap);
						}
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		// System.err.println(dataMap);
		return dataMap;
	}

	public static void checkElement(String name, Element elem) {
		if (elem == null) {
			throw new RuntimeException("Unable to find " + name);
		}
	}

}
