package com.web.scarping.service;

import java.io.File;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class JavaMailsenderService {
	@Autowired
	private JavaMailSender javaMailSender;

	@Value("${app.variable.emailid.notification}")
	private String from;

	@Value("${pdf.location}")
	private String PDF_LOCATION;

	public void sendMailWithAttachment(String to, String subject, String body, String fileToAttach) {
		log.info("to, :: {}", to);
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
				mimeMessage.setFrom(new InternetAddress(from));
				mimeMessage.setSubject(subject);
				//mimeMessage.setText(body);
				
				DataSource source = new FileDataSource(PDF_LOCATION + fileToAttach);
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				MimeBodyPart textMessage = new MimeBodyPart();
				textMessage.setText(body);
				
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(fileToAttach);
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);
				multipart.addBodyPart(textMessage);
				mimeMessage.setContent(multipart);
				
			}
		};

		try {
			javaMailSender.send(preparator);
		} catch (MailException ex) {
			// simply log it and go on...
			System.err.println(ex.getMessage());
		}
	}

	public void sendMail() {

		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("ashish.katiyar2018@gmail.com");
		message.setTo("ashish_katiyar16@rediffmail.com");
		message.setSubject("Safer data Test");
		message.setText("Hi Ashish \n How Are ?");
		javaMailSender.send(message);
	}

}
