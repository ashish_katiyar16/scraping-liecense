package com.web.scarping.service;

import java.util.Map;

public interface SearchMCLicentService {
	public Map<String, String> getLicenceDetails(String searchKey);
}
