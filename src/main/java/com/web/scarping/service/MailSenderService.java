package com.web.scarping.service;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {
	
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Value("${app.variables.smtp.port}")
	private String port;
	
	@Value("${app.variables.smtp.host}")
	private String host;
	
	@Value("${app.variable.emailid.notification}")
	private String from;
	
	
	boolean sendMail(String to, String message, String subject){
		boolean result = false;
		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setFrom(from);
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(message, true);
			javaMailSender.send(msg);
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
		
	}
	
	boolean sendMailBySmtp(String to, String mailContent, String subject){
		boolean result = false;
		Properties props = null;
		Transport transport = null;
		Session mailSession = null;
		MimeMessage message = null;
		try {
			props = new Properties(System.getProperties());
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.host", host);
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			props.put("mail.smtp.from", "<" + from + ">");
			mailSession = Session.getInstance(props);
			mailSession.setDebug(false);
			message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(from));
			InternetAddress[] replyToAddress = new InternetAddress[1];
			replyToAddress[0] = new InternetAddress(from);
			message.setReplyTo(replyToAddress);
			message.addRecipient(Message.RecipientType.TO,
					new InternetAddress(to));
			message.setSubject(subject, "UTF-8");
			message.setSentDate(new Date());
			message.setContent(mailContent, "text/HTML; charset=utf-8");
			message.setHeader("Content-Type", "text/html; charset=utf-8");
			message.setHeader("Content-Transfer-Encoding", "quoted-printable");
			message.saveChanges();
			transport = mailSession.getTransport();
			transport.connect();
			message.saveChanges();
			transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
			transport.close();
			transport = null;
			mailSession = null;
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

}
