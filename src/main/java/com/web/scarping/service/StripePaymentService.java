package com.web.scarping.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.web.scarping.dao.PaymentRequest;
import com.web.scarping.dto.ChargeRequest;

@Service
public class StripePaymentService {

	
	@Value("${stripe.secret}")
	private String STRIPE_SECRET_KEY;
	
    @PostConstruct
    public void init() {
        Stripe.apiKey = STRIPE_SECRET_KEY;
    }
	public Charge charge(ChargeRequest chargeRequest) throws StripeException {
		 Map<String, Object> chargeParams = new HashMap<>();
	        chargeParams.put("amount", chargeRequest.getAmount());
	        chargeParams.put("currency", chargeRequest.getCurrency());
	        chargeParams.put("description", chargeRequest.getDescription());
	        chargeParams.put("source", chargeRequest.getStripeToken());
	        return Charge.create(chargeParams);
	}
	
	
}