package com.web.scarping.service;

import com.paypal.api.payments.Payment;
import com.stripe.model.Charge;
import com.web.scarping.dto.ChargeRequest;
import com.web.scarping.dto.Order;
import com.web.scarping.dto.OrderResponseDTO;
import com.web.scarping.model.PaypalPaymentModel;

public interface PaymentService {
	public OrderResponseDTO getOrderdetails(String pId);

	public String updateOrderStatus(String id, String paypalResponse, String status, String paymentId);

	public void createPaypalPayment(Order order, String id);

	public PaypalPaymentModel getPaypalPaymentDetails(String paymentId);

	public void updatePaypalPayment(String id, Payment payment);

	public void sendEmailtoUser(String paymentId);

	public void updateStripePayment(String id, Charge charge);

	public PaypalPaymentModel createPaypalPaymentStripe(Charge charge, ChargeRequest chargeRequest);

	public void savepdf(String id);
}
