package com.web.scarping.service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReadMasterDataServiceImpl implements ReadMasterDataService {
	@Value("${mc.master.url}")
	private String masterURl;

	public List<String> getLicenceData() {
		List<String> licenseList = new LinkedList<String>();
		Document response = null;
		try {
			response = Jsoup.connect(masterURl).get();
			// System.out.println(response);
			Elements elements = response.getElementsByTag("a");
			for (Element aTag : elements) {
				String lcNumber = aTag.ownText();
				// log.info(lcNumber);
				// System.out.println(lcNumber);
				if (lcNumber != null && lcNumber.contains("MC")) {
					licenseList.add(lcNumber);
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return licenseList;
	}
}
