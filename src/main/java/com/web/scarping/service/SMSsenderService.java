package com.web.scarping.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service

public class SMSsenderService {
	@Autowired
	private AmazonSNSClient snsClient;

	
	public PublishResult sendSMSMessage(String message, String phoneNumber) {
		PublishResult result = null;
		try {
			Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
			smsAttributes.put("AWS.SNS.SMS.SMSType",
					new MessageAttributeValue().withStringValue("Transactional").withDataType("String"));
			result = snsClient.publish(new PublishRequest().withMessage(message).withPhoneNumber(phoneNumber));
			log.info("Message sent to Phone: {} with message content: {} and Message Response Id: {}", phoneNumber,
					message, result.getMessageId());
			Thread.sleep(1000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
