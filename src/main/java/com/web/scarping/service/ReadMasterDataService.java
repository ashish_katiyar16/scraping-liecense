package com.web.scarping.service;

import java.util.List;

public interface ReadMasterDataService {
	public List<String> getLicenceData();
}
