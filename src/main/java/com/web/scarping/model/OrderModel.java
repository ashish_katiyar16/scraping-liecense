package com.web.scarping.model;

import java.time.LocalDate;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@DynamoDBTable(tableName = "order_master_new")
public class OrderModel {

	@DynamoDBHashKey
	private String id;

	@DynamoDBAttribute
	private String mcId;

	@DynamoDBAttribute
	private String paymentId;

	@DynamoDBAttribute

	private String expiryDate;

	@DynamoDBAttribute
	private String email;

	@DynamoDBAttribute
	private String paypalResponse;

	@DynamoDBAttribute
	private String status = "PENDING";

}
