package com.web.scarping.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@DynamoDBTable(tableName = "stripe_payment")
public class PaypalPaymentModel {

	@DynamoDBHashKey
	private String id;

	@DynamoDBAttribute
	private String email;

	@DynamoDBAttribute
	private String orderId;
	
	@DynamoDBAttribute
	private String transactionId;
	
	@DynamoDBAttribute
	private String paymentMethod;
	
	@DynamoDBAttribute
	private String paymentDate;

	@DynamoDBAttribute
	private String status = "INITIATED";

}
