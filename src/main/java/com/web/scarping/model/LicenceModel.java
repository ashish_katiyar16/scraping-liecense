package com.web.scarping.model;

import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Component
@DynamoDBTable(tableName = "licence_master_new")
public class LicenceModel {

	@DynamoDBHashKey
	private String mcId;

	@DynamoDBAttribute
	private String legalName;

	@DynamoDBAttribute
	private String physicalAddress;

	@DynamoDBAttribute
	private String mailingaddress;

	@DynamoDBAttribute
	private String phone;

	@DynamoDBAttribute
	private String usDot;

	@DynamoDBAttribute
	private String approvalDate;

}
