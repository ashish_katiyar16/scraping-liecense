package com.web.scarping.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.html2pdf.HtmlConverter;

public class HTMLTOPdfConverterUtil {
	public static void htmlToPdfConverter(String htmlContent, String pdfFileName) {
		try {
			HtmlConverter.convertToPdf(htmlContent, new FileOutputStream(pdfFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
