package com.web.scarping.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Util {
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	public static String getFormattedDate(String date) {
		String strDate = null;
		try {
			if (date == null) {
				return null;
			}
			LocalDate parse = LocalDate.parse(date);
			strDate = parse.format(DateTimeFormatter.ofPattern("MMMM dd, yyyy"));
			return strDate;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;
	}

	public static String getCurrentDate(int nofDays) {
		String currentDate = null;
		try {
			LocalDate localDate = LocalDate.now();
			localDate = localDate.plusDays(nofDays);
			currentDate = "" + localDate;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentDate;
	}

//	public static String getCurrentDateString() {
//		String dateString = "";
//		Date date = new Date();
//		dateString = formatter.format(date);
//		return dateString;
//	}

	public static boolean compareDate(LocalDate firstDate, LocalDate secondDate) {
		boolean isTrue = false;
		try {
			isTrue = firstDate.isAfter(secondDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isTrue;
	}

	public static String generatePaymentId() {
		return String.valueOf(System.currentTimeMillis());
	}

	public static String formatMailingAddress(String address) {
		String finalAddress = null;
		String arr[] = address.split(" ");
		if (arr.length > 4) {
			finalAddress = arr[arr.length - 3] + " " + arr[arr.length - 2];
		}
		return finalAddress;
	}
	
	public static String formatPhoneNumber(String p){
		p = p.replaceAll("\\(", "");
		p = p.replaceAll("\\)", "");
		p = p.replaceAll(" ", "");
		p = p.replaceAll("-", "");
		p="+1"+p;
		return p;
	}

	public static void main(String[] args) {
		System.out.println("For Date 2021-07-30 : "+checkApprovalDate("2021-07-30"));
		System.out.println(formatPhoneNumber("(301) 780-3432"));
		String messagetemplate = "Hi <businessname> \n Congratulations on the approval of your motor carrier authority. Did you know you can receive your certificate instantly rather than waiting 4 to 10 business days? Click the link to receive your Certificate instantly <link>";
		String stringBuilder = messagetemplate.replaceAll("<businessname>","IN & OUT TRANSPORT LLC");
		String linkBaseURL = "https://www.truckersfinder.com/payment?id=";
		String finalLink = linkBaseURL.concat("744394429");
		stringBuilder = stringBuilder.replaceAll("<link>", finalLink);
		System.out.println(stringBuilder);
		//System.out.println(generatePaymentId());
		//System.out.println("" + Util.getCurrentDate(7));
		//System.out.println(compareDate(LocalDate.parse(getCurrentDate(1)), LocalDate.parse(getCurrentDate(0))));
	}


	public static boolean checkApprovalDate(String approvalDate) {
		boolean isTrue = false;
		try {
			LocalDate approvalDateF = LocalDate.parse(approvalDate);
			LocalDate allowedDate = LocalDate.parse(Util.getCurrentDate(-5));
			isTrue = approvalDateF.isAfter(allowedDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isTrue;
	}
}
