package com.web.scarping.dto;

import lombok.Data;

@Data
public class ResponseDTO {
	private String status;
	private int statusCode;

}
