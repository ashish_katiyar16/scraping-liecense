package com.web.scarping.dto;

import java.time.LocalDate;

import com.web.scarping.model.OrderModel;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Order {
    
	private String orderId;
	private String emailId;
	private double price=14.99;
	private String currency="USD";
	private String method="Paypal";
	private String intent="sale";
	private String description="MC Licence Payment";
	

	
}
