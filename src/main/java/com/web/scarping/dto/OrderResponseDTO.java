package com.web.scarping.dto;

import lombok.Data;

@Data
public class OrderResponseDTO {
	private String paymentId;
	private String orderId;
	private String mcId;

	private String legalName;
	private String usDot;

	private String mailingaddress;
	private String physicalAddress;

	private String phone;
	private String status;
	private String approveDate;

}
