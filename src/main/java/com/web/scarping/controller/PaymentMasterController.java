package com.web.scarping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.web.scarping.dto.OrderResponseDTO;
import com.web.scarping.service.PaymentService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class PaymentMasterController {
	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "/getLicenceDetails", method = RequestMethod.GET)
	public ResponseEntity<OrderResponseDTO> getPaydetail(@RequestParam("id") String id) {
		log.info("Request Id {}", id);
		ResponseEntity<OrderResponseDTO> responseEntity = null;
		OrderResponseDTO paymentdetails = paymentService.getOrderdetails(id);
		if (paymentdetails != null) {
			responseEntity = new ResponseEntity<OrderResponseDTO>(paymentdetails, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<OrderResponseDTO>(paymentdetails, HttpStatus.NOT_FOUND);

		}
		return responseEntity;
	}

	@RequestMapping(value = "/updatePayment", method = RequestMethod.POST)
	public ResponseEntity<String> updatePayment(@RequestParam String id, @RequestParam String paypalResponse,
			@RequestParam String status) {
		log.info("Request Id {}", id);
		String result = paymentService.updateOrderStatus(id, paypalResponse, status,id);
		if ("FAILED".equalsIgnoreCase(result)) {
			return new ResponseEntity<String>(result, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<String>(result, HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/getPDF", method = RequestMethod.GET)
	public OrderResponseDTO getPDF(@RequestParam String id) {
		log.info("Request Id {}", id);
		return paymentService.getOrderdetails(id);

	}

}
