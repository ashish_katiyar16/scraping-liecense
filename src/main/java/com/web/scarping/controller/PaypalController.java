package com.web.scarping.controller;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lowagie.text.DocumentException;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.web.scarping.dao.UserRepository;
import com.web.scarping.dto.ChargeRequest;
import com.web.scarping.dto.ChargeRequest.Currency;
import com.web.scarping.dto.Order;
import com.web.scarping.dto.OrderResponseDTO;
import com.web.scarping.dto.PdfDto;
import com.web.scarping.model.OrderModel;
import com.web.scarping.model.PaypalPaymentModel;
import com.web.scarping.model.User;
import com.web.scarping.service.PaymentService;
import com.web.scarping.service.PaypalService;
import com.web.scarping.service.StripePaymentService;
import com.web.scarping.util.Util;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class PaypalController {

	@Autowired
	private PaypalService service;

	@Autowired
	private PaymentService paymentService;

	@Value("${paypal.payment.success.url}")
	private String SUCCESS_URL;

	@Value("${paypal.payment.cancel.url}")
	private String CANCEL_URL;

	@Value("${stripe.publishable}")
	private String STRIPE_PUBLISH_KEY;

	@Value("${stripe.secret}")
	private String STRIPE_SECRET_KEY;

	@Value("${pdf.location}")
	private String PDF_LOCATION;

	@Autowired
	private StripePaymentService stripePaymentService;
	@Autowired
	private DynamoDBMapper dynamoDBMapper;
	

	@GetMapping("/")
	public String home() {
		User user = dynamoDBMapper.load(User.class, "12345");
		System.out.println(user);
		return "home";
	}
	
	@GetMapping("/register")
	public String register() {
		return "example";
	}

	@GetMapping("/payment")
	public String payment(@RequestParam("id") String id, Model model) {
		OrderResponseDTO orderDetails = paymentService.getOrderdetails(id);
		model.addAttribute("orderId", id);
		if (orderDetails != null) {
			if (orderDetails.getStatus().equalsIgnoreCase("EXPIRED")) {
				return "expired";
			} else if (orderDetails.getStatus().equalsIgnoreCase("SUCCESS")) {
				model.addAttribute("paymentId", orderDetails.getPaymentId());
				model.addAttribute("orderId", orderDetails.getOrderId());
				return "success";
			} else {
				model.addAttribute("legalName", orderDetails.getLegalName());
				model.addAttribute("mcId", orderDetails.getMcId());
				model.addAttribute("usDot", orderDetails.getUsDot());
				model.addAttribute("mailingaddress", orderDetails.getMailingaddress());
				model.addAttribute("physicalAddress", orderDetails.getPhysicalAddress());
				model.addAttribute("phone", orderDetails.getPhone());
				model.addAttribute("stripePublicKey", STRIPE_PUBLISH_KEY);
				model.addAttribute("amount", 1499);
				model.addAttribute("currency", "USD");
				return "payment";
			}
		}
		return "redirect:/";

	}

	@PostMapping("/charge")
	public String charge(ChargeRequest chargeRequest, Model model) throws StripeException {
		try {
			chargeRequest.setDescription("Licence Purchase checkout");
			chargeRequest.setCurrency(Currency.USD);
			chargeRequest.setAmount(1499);
			Charge charge = stripePaymentService.charge(chargeRequest);
			if (charge != null) {
				if (charge.getPaid()) {
					if (charge.getStatus().equalsIgnoreCase("succeeded")) {
						// model.addAttribute("status", charge.getStatus());
						model.addAttribute("paymentId", charge.getId());
						PaypalPaymentModel paypalPaymentModel = paymentService.createPaypalPaymentStripe(charge,
								chargeRequest);
						paymentService.updateStripePayment(paypalPaymentModel.getId(), charge);
						paymentService.savepdf(charge.getId());
						paymentService.sendEmailtoUser(charge.getId());
						return "redirect:/paySuccess";
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "cancel";
	}

	@GetMapping(value = "paySuccess")
	public String paySuccess(@RequestParam("paymentId") String paymentId, Model model) {
		model.addAttribute("paymentId", paymentId);
		return "success";
	}

	@RequestMapping(value = "/createPayment", method = RequestMethod.POST)
	public String payment(@ModelAttribute Order order) {
		try {
			System.out.println("Order Details : " + order);

			Payment payment = service.createPayment(order.getPrice(), order.getCurrency(), order.getMethod(),
					order.getIntent(), order.getDescription(), CANCEL_URL, SUCCESS_URL, order.getOrderId());
			paymentService.createPaypalPayment(order, payment.getId());
			for (Links link : payment.getLinks()) {
				if (link.getRel().equals("approval_url")) {
					return "redirect:" + link.getHref();
				}
			}

		} catch (PayPalRESTException e) {

			e.printStackTrace();
		}
		return "redirect:/";
	}

	@GetMapping(value = "cancel")
	public String cancelPay() {
		return "cancel";
	}

	@GetMapping(value = "success")
	public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId,
			Model model) {
		try {
			Payment payment = service.executePayment(paymentId, payerId);
			System.out.println(payment.toJSON());
			if (payment.getState().equals("approved")) {
				PaypalPaymentModel paypalPaymentModel = paymentService.getPaypalPaymentDetails(paymentId);
				if (paypalPaymentModel != null) {
					paymentService.updateOrderStatus(paypalPaymentModel.getOrderId(), payment.getState(), "SUCCESS",
							paypalPaymentModel.getId());
					paymentService.updatePaypalPayment(paypalPaymentModel.getId(), payment);
				}
				model.addAttribute("paymentId", paymentId);
				paymentService.sendEmailtoUser(paymentId);
				return "success";
			} else {
				PaypalPaymentModel paypalPaymentModel = paymentService.getPaypalPaymentDetails(paymentId);
				if (paypalPaymentModel != null) {
					paymentService.updateOrderStatus(paypalPaymentModel.getOrderId(), payment.getState(), "FAILURE",
							paymentId);
				}
				return "cancel";
			}
		} catch (PayPalRESTException e) {
			System.out.println(e.getMessage());
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/downloadPdf", method = RequestMethod.POST)
	public String pdf(@ModelAttribute PdfDto dto, Model model, HttpServletResponse response) {
		PaypalPaymentModel paypalPaymentModel = paymentService.getPaypalPaymentDetails(dto.getPaymentId());
		if (paypalPaymentModel != null) {
			if (paypalPaymentModel.getStatus().equalsIgnoreCase("approved")) {
				return "redirect:/dwncrt?id="+paypalPaymentModel.getOrderId();
			}

		}
		return "redirect:/";
	}

	/*
	 * @GetMapping("/dwncrt") public String pdfLinkEmail(@RequestParam("id") String
	 * id, Model model) { OrderResponseDTO paymentResponseDTO =
	 * paymentService.getOrderdetails(id); if (paymentResponseDTO != null &&
	 * paymentResponseDTO.getStatus().equalsIgnoreCase("SUCCESS")) {
	 * model.addAttribute("approveDate",
	 * Util.getFormattedDate(paymentResponseDTO.getApproveDate()));
	 * model.addAttribute("mcId", "MC-" + paymentResponseDTO.getMcId() + "-C");
	 * model.addAttribute("usdotNo", "U.S. DOT No. " +
	 * paymentResponseDTO.getUsDot()); model.addAttribute("leagalName",
	 * paymentResponseDTO.getLegalName()); model.addAttribute("address",
	 * Util.formatMailingAddress(paymentResponseDTO.getPhysicalAddress()));
	 * model.addAttribute("logoImage", "data:image/png;base64, " +
	 * convertToBase64("logo.png")); model.addAttribute("signImage",
	 * "data:image/png;base64, " + convertToBase64("signature.png")); return
	 * "updatedpdf"; } return "redirect:/"; }
	 */

	@Autowired
	private TemplateEngine templateEngine;

	@GetMapping("/testPdf")
	public void testPdf(Model model, HttpServletResponse response) throws IOException {
		Context ctx = new Context();
		ctx.setVariable("approveDate", Util.getFormattedDate("2021-08-29"));
		ctx.setVariable("mcId", "MC-" + "1242328" + "-C");
		ctx.setVariable("usdotNo", "U.S. DOT No. " + "3588493");
		ctx.setVariable("leagalName", "IN & OUT TRANSPORT LLC");
		ctx.setVariable("address", Util.formatMailingAddress("8610 WILD BASIN DRIVE HOUSTON, TX 77088"));
		ctx.setVariable("logoImage", "data:image/png;base64, " + convertToBase64("logo.png"));
		ctx.setVariable("signImage", "data:image/png;base64, " + convertToBase64("signature.png"));
		String processedHtml = templateEngine.process("updatedpdf", ctx);
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		ITextRenderer renderer = new ITextRenderer();
		renderer.setDocumentFromString(processedHtml);
		renderer.layout();
		try {
			renderer.createPDF(out, false);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		renderer.finishPDF();
		FileOutputStream fout = new FileOutputStream(PDF_LOCATION + "MC-1242328-C.pdf");
		fout.write(out.toByteArray());
		fout.close();
		Path file = Paths.get(PDF_LOCATION, "MC-1242328-C.pdf");
		try {
			Files.copy(file, response.getOutputStream());
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-disposition", "attachment; filename=MC-1242328-C.pdf");
			response.getOutputStream().flush();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@GetMapping(value = "dwncrt")
	public void downloadPdf(@RequestParam("id") String id, Model model, HttpServletResponse response) {
		OrderResponseDTO paymentResponseDTO = paymentService.getOrderdetails(id);
		if (paymentResponseDTO != null) {
			String fileName = "MC-" + paymentResponseDTO.getMcId() + "-C.pdf";
			String outPutFileName = fileName;
			//response.setContentType("application/pdf");
			response.setCharacterEncoding("utf-8");
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", outPutFileName);
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader(headerKey, headerValue);
			Path file = Paths.get(PDF_LOCATION, fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * images --- GET request to display image
	 * 
	 * @param id       -- Image file name
	 * 
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/images/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public void getImage(@PathVariable String id, HttpServletResponse response) throws IOException {

		try {
			ClassPathResource imgFile = new ClassPathResource("/" + id);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
		} catch (Exception e) {
			log.info("Exception in getImage Method : " + e.getMessage());
			e.printStackTrace();
		}

	}
	
	/**
	 * images --- GET request to display image
	 * 
	 * @param id       -- Image file name
	 * 
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/assets/{id}", method = RequestMethod.GET, produces = MediaType.ALL_VALUE)
	public void assets(@PathVariable String id, HttpServletResponse response) throws IOException {

		try {
			ClassPathResource imgFile = new ClassPathResource("/" + id);
			response.setContentType(MediaType.ALL_VALUE);
			StreamUtils.copy(imgFile.getInputStream(), response.getOutputStream());
		} catch (Exception e) {
			log.info("Exception in getImage Method : " + e.getMessage());
			e.printStackTrace();
		}

	}

	private String convertToBase64(String fileName) {
		byte[] imageAsBytes = new byte[0];
		try {
			ClassPathResource imgFile = new ClassPathResource("/" + fileName);
			InputStream inputStream = imgFile.getInputStream();
			imageAsBytes = IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			System.out.println("\n File read Exception");
		}
		return Base64.getEncoder().encodeToString(imageAsBytes);
	}

}
