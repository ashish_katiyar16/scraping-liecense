package com.web.scarping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class WebScrapingApplication {
	private static Logger logger = LoggerFactory.getLogger(WebScrapingApplication.class);

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(WebScrapingApplication.class, args);
		logger.info("info");
	}


}
