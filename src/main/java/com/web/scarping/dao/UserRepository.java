package com.web.scarping.dao;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.web.scarping.model.User;

import java.util.List;
 
//annotation enables the scan operations
@EnableScan
//spring annotation
@Component
public interface UserRepository extends CrudRepository<User, String> {
 
    @EnableScanCount
    long countByEmail(String genre);
 
    List<User> findAllByEmail(String genre);
}