package com.web.scarping.dao;

import com.web.scarping.model.PaypalPaymentModel;

public interface PaypalPaymentRepo {

	public PaypalPaymentModel save(PaypalPaymentModel paymentModel);

	public PaypalPaymentModel update(PaypalPaymentModel paymentModel);

	public PaypalPaymentModel getPayment(String pId);
}
