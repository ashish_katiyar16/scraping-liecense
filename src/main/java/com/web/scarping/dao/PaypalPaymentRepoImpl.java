package com.web.scarping.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.web.scarping.model.OrderModel;
import com.web.scarping.model.PaypalPaymentModel;

@Repository
public class PaypalPaymentRepoImpl implements PaypalPaymentRepo {
	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public PaypalPaymentModel save(PaypalPaymentModel paymentModel) {
		dynamoDBMapper.save(paymentModel);
		return paymentModel;
	}

	public PaypalPaymentModel getPayment(String id) {
		return dynamoDBMapper.load(PaypalPaymentModel.class, id);
	}

	public PaypalPaymentModel update(PaypalPaymentModel paymentModel) {
		DynamoDBMapperConfig dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
				.withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT)
				.withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE).build();
		dynamoDBMapper.save(paymentModel, dynamoDBMapperConfig);
		return paymentModel;
	}

}
