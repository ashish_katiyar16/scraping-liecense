package com.web.scarping.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.web.scarping.model.LicenceModel;

@Repository
public class LicenceRepoImpl implements LicenceRepo {

	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public LicenceModel save(LicenceModel licenceModel) {
		dynamoDBMapper.save(licenceModel);
		return licenceModel;
	}

	public LicenceModel getLicence(String mcId) {
		LicenceModel load = dynamoDBMapper.load(LicenceModel.class, mcId);
		return load;
	}
//	@Override
//	public List<LicenceModel> get(String mcId) {
////		LicenceModel load = dynamoDBMapper.load(LicenceModel.class, mcId);
////		System.out.println(load);
////		return null;
//
//		try {
//			Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
//			eav.put(":v1", new AttributeValue().withS(mcId));
//
//			ScanRequest scanRequest = new ScanRequest().withTableName("licence").withFilterExpression("mcId = :val")
//					.withProjectionExpression("mcId").withExpressionAttributeValues(eav);
//
//			ScanResult result = amazonDynamoDB.scan(scanRequest);
//			for (Map<String, AttributeValue> item : result.getItems()) {
//				System.out.println(item);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//	@Override
//	public List<LicenceModel> get(String mcId) {
//		GetItemSpec spec = new GetItemSpec().withPrimaryKey("id", mcId);
//
//		try {
//			Table table = dynamoDB.getTable("licence");
//			System.out.println("Attempting to read the item...");
//			Item outcome = table.getItem(spec);
//			System.out.println("GetItem succeeded: " + outcome);
//
//		} catch (Exception e) {
//			System.err.println(e.getMessage());
//		}
//
//		return null;
//	}

	public List<LicenceModel> get(String mcId) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":v1", new AttributeValue().withS(mcId));
		DynamoDBQueryExpression<LicenceModel> queryExpression = new DynamoDBQueryExpression<LicenceModel>()
				.withKeyConditionExpression("mcId = :v1").withExpressionAttributeValues(eav);
		List<LicenceModel> latestReplies = dynamoDBMapper.query(LicenceModel.class, queryExpression);
		return latestReplies;
	}

//	 @Override
// 	public void listTables() {
// 
// 		DynamoDB dynamoDB = new DynamoDB(dynamoDbClient);
// 
// 		TableCollection<ListTab> tables = dynamoDB.listTables();
// 		Iterator<Table> iterator = tables.iterator();
// 
// 		while (iterator.hasNext()) {
// 			Table table = iterator.next();
// 			System.out.println(table.getTableName());
// 		}
// 
// 	}
}
