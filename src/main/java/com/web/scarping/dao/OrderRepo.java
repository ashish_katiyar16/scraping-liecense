package com.web.scarping.dao;

import com.web.scarping.model.OrderModel;

public interface OrderRepo {
	public OrderModel save(OrderModel paymentModel);

	public OrderModel update(OrderModel paymentModel);

	public OrderModel getOrder(String pId);

}
