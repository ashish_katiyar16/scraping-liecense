package com.web.scarping.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.web.scarping.model.OrderModel;

@Repository
public class OrderRepoImpl implements OrderRepo {
	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public OrderModel save(OrderModel paymentModel) {
		dynamoDBMapper.save(paymentModel);
		return paymentModel;
	}

	public OrderModel getOrder(String id) {
		return dynamoDBMapper.load(OrderModel.class, id);
	}

	public OrderModel update(OrderModel paymentModel) {
		DynamoDBMapperConfig dynamoDBMapperConfig = new DynamoDBMapperConfig.Builder()
				.withConsistentReads(DynamoDBMapperConfig.ConsistentReads.CONSISTENT)
				.withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE).build();
		dynamoDBMapper.save(paymentModel, dynamoDBMapperConfig);
		return paymentModel;
	}

}
