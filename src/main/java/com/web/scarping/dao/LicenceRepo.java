package com.web.scarping.dao;

import com.web.scarping.model.LicenceModel;

public interface LicenceRepo {
//	public void createTable(String tableName);
//
//	public void listTables();
	public LicenceModel save(LicenceModel licenceModel);

	public LicenceModel getLicence(String mcId);

}
