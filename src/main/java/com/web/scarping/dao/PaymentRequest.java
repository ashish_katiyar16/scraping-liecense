package com.web.scarping.dao;

import com.stripe.model.Token;

public class PaymentRequest {

	   public enum Currency{
		INR,USD,AUD;
	   }
	    private String description;
	    private double amount=14.99;
	    private Currency currency;
	    private String stripeEmail;
	    private Token token;
	    private int quizGroupId;
	    private int quizUserId;
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public double getAmount() {
			return amount;
		}
		public void setAmount(double amount) {
			this.amount = amount;
		}
		public Currency getCurrency() {
			return currency;
		}
		public void setCurrency(Currency currency) {
			this.currency = currency;
		}
		public String getStripeEmail() {
			return stripeEmail;
		}
		public void setStripeEmail(String stripeEmail) {
			this.stripeEmail = stripeEmail;
		}
		public Token getToken() {
			return token;
		}
		public void setToken(Token stripeToken) {
			this.token = stripeToken;
		}
		public int getQuizGroupId() {
			return quizGroupId;
		}
		public void setQuizGroupId(int quizGroupId) {
			this.quizGroupId = quizGroupId;
		}
		public int getQuizUserId() {
			return quizUserId;
		}
		public void setQuizUserId(int quizUserId) {
			this.quizUserId = quizUserId;
		}

	    
	    
}